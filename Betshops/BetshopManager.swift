//
//  BetshopManager.swift
//  Betshops
//
//  Created by Patrik Durdevic on 25/10/2019.
//  Copyright © 2019 Patrik Durdevic. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol BetshopManagerDelegate {
    func didUpdateBetshops(betshops: [Betshop])
    func didFailToLoadBetshops()
    func didStartLoadingData()
}

class BetshopManager: NSObject {
    
    var betshops: [Betshop] = []
    var timeZoneForBetshop: [Int: TimeZone] = [Int: TimeZone]()
    var delegate: BetshopManagerDelegate?
    
    override init() {
        super.init()
    }
    
    // This function is used to download all the betshops
    func downloadBetshops() {
        delegate?.didStartLoadingData()
        
        // Update coordinates to be the rectangle around germany
        let requestURL = URL(string: "https://scorealarm-v4-staging-oddset-api.azurewebsites.net/api/OMh3VFFoNEq8c4Ql/1/de/util/shops?boundingBox=55.059809,15.042221,47.270135,5.865860")
        
        URLSession.shared.dataTask(with: requestURL!) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                print(error)
                self.delegate?.didFailToLoadBetshops()
                
                return
            }
            
            if let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data {
                self.parseBetshopData(data: data)
            } else {
                self.delegate?.didFailToLoadBetshops()
            }
        }.resume()
    }
    
    // This function parses the JSON data we get from the server and transforms it into an array of Betshop objects. Furthermore, it detects the timezone of each betshop so that we can accurately show when the betshop is open and when it's closed
    func parseBetshopData(data: Data) {
        do {
            betshops = try JSONDecoder().decode([Betshop].self, from: data)
            for betshop in betshops {
                // Since we are required by documentation to display only the betshops in Germany, we can immediately set the TimeZone to "Europe/Berlin", otherwise we would need to use the commented code
                /*CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: betshop.Location.Lat, longitude: betshop.Location.Lng), completionHandler: { placemarks, error in
                    if let placemark = placemarks?.first {
                        self.timeZoneForBetshop[betshop.Id] = placemark.timeZone
                    }
                })*/
                self.timeZoneForBetshop[betshop.Id] = TimeZone(identifier: "Europe/Berlin")
            }
            self.delegate?.didUpdateBetshops(betshops: betshops)
        } catch let error {
            print(error)
            self.delegate?.didFailToLoadBetshops()
        }
    }
    
    // This function gets all the annotations for the betshops
    func getAnnotations() -> [MKAnnotation] {
        var annotations:[MKAnnotation] = []
        var alreadyShown:[BetshopLocation:Bool] = [:]
        for betshop in betshops {
            if let _ = alreadyShown[betshop.Location] { continue }
            alreadyShown[betshop.Location] = true
            let annotation = BetshopAnnotation(betshop: betshop)
            annotations.append(annotation)
        }
        return annotations
    }
}
