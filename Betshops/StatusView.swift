//
//  StatusView.swift
//  Betshops
//
//  Created by Tea Durdevic on 26/10/2019.
//  Copyright © 2019 Patrik Durdevic. All rights reserved.
//

import UIKit

enum StatusStates {
    case None
    case LoadingData
    case NoInternet
}

protocol StatusViewDelegate {
    func userRequestedReload()
}

class StatusView: UIView {

    var delegate: StatusViewDelegate?
    
    @IBOutlet weak var statusTitleLabel: UILabel!
    @IBOutlet weak var statusActionLabel: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
    
    var state: StatusStates = .None
    
    func setState(state: StatusStates) {
        self.state = state
        DispatchQueue.main.async {
            self.updateState()
        }
    }
    
    // Update the state of the status view
    func updateState() {
        if state == .LoadingData {
            UIView.transition(with: self, duration: 0.5, options: .curveEaseInOut, animations: {
                self.statusTitleLabel.text = "Loading..."
                self.statusActionLabel.text = "Tap to dismiss"
                self.statusActionLabel.alpha = 1
                self.statusImageView.image = UIImage(named: "internet")
                self.backgroundColor = UIColor(red: 139/255, green: 188/255, blue: 21/255, alpha: 1)
            }, completion: { value in
                UIView.animate(withDuration: 0.5, animations: {
                    self.alpha = 1
                }, completion: { value in
                    self.statusActionLabel.alpha = 1
                    UIView.animate(withDuration: 1, delay: 0, options: [.repeat, .autoreverse], animations: {
                        self.statusActionLabel.alpha = 0
                    })
                })
            })
        } else {
            UIView.transition(with: self, duration: 0.5, options: .curveEaseInOut, animations: {
                self.statusTitleLabel.text = "No internet connection"
                self.statusActionLabel.text = "Tap to try again"
                self.statusActionLabel.alpha = 1
                self.statusImageView.image = UIImage(named: "error")
                self.backgroundColor = UIColor(red: 245/255, green: 108/255, blue: 108/255, alpha: 1)
            }, completion: { value in
                UIView.animate(withDuration: 0.5, animations: {
                    self.alpha = 1
                }, completion: { value in
                    self.statusActionLabel.alpha = 1
                    UIView.animate(withDuration: 1, delay: 0, options: [.repeat, .autoreverse], animations: {
                        self.statusActionLabel.alpha = 0
                    })
                })
            })
        }
    }
    
    // Hide the status view, animated
    func hide() {
        state = .None
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.alpha = 0
            })
        }
    }
    
    // Tap to dismiss and tap to reload functionality
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if state == .NoInternet {
            delegate?.userRequestedReload()
        }
        hide()
    }
}
