//
//  InfoView.swift
//  Betshops
//
//  Created by Patrik Durdevic on 25/10/2019.
//  Copyright © 2019 Patrik Durdevic. All rights reserved.
//

import UIKit

class InfoView: UIView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityCountyLabel: UILabel!
    
    @IBOutlet weak var telephoneLabel: UILabel!
    
    @IBOutlet weak var openedUntilLabel: UILabel!
    
    @IBOutlet weak var routeButton: UIButton!
    
    var selectedBetshop: Betshop?
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    // Present a new infoView, update the information and call show()
    func present(betshop: Betshop, calendar: Calendar) {
        nameLabel.text = betshop.Name.trimmingCharacters(in: .whitespaces)
        addressLabel.text = betshop.Address
        cityCountyLabel.text = betshop.City + " - " + betshop.County
        
        // The telephone number is not defined in the JSON, hence I'm showing the one from the provided screenshot
        telephoneLabel.text = "311-56-44"
        
        let date = Date()
        
        let hour = calendar.component(.hour, from: date)
        if hour >= 16 || hour < 8 {
            openedUntilLabel.text = "Opens tomorrow at 08:00"
        } else {
            openedUntilLabel.text = "Open now until 16:00"
        }
        
        selectedBetshop = betshop
        show()
    }
    
    // Show the infoView, animated
    func show() {
        UIView.animate(withDuration: 0.5, animations: {
            self.topConstraint.constant = -self.frame.height + 25
            self.superview?.layoutIfNeeded()
        })
    }
    
    // Hide the infoView, animated
    func hide() {
        UIView.animate(withDuration: 0.5, animations: {
            self.topConstraint.constant = 0
            self.superview?.layoutIfNeeded()
        })
    }
}
