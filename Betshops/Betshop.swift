//
//  Betshop.swift
//  Betshops
//
//  Created by Patrik Durdevic on 25/10/2019.
//  Copyright © 2019 Patrik Durdevic. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

struct Betshop: Codable {
    let Features: [Int]
    let Id: Int
    let Name: String
    let Location: BetshopLocation
    let Address: String
    let CityId: Int
    let City: String
    let County: String
}

struct BetshopLocation: Codable, Equatable, Hashable {
    var Lat: Double
    var Lng: Double
    
    static func == (lhs: BetshopLocation, rhs: BetshopLocation) -> Bool {
        return lhs.Lat == rhs.Lat && lhs.Lng == rhs.Lng
    }
}

class BetshopAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var betshop: Betshop
    
    init(betshop: Betshop) {
        self.betshop = betshop
        self.title = betshop.Name
        self.coordinate = CLLocationCoordinate2D(latitude: betshop.Location.Lat, longitude: betshop.Location.Lng)
    }
}
