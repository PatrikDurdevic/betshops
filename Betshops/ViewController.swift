//
//  ViewController.swift
//  Betshops
//
//  Created by Patrik Durdevic on 25/10/2019.
//  Copyright © 2019 Patrik Durdevic. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, BetshopManagerDelegate, StatusViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate {

    var betshopManager: BetshopManager?
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var infoView: InfoView!
    @IBOutlet weak var statusView: StatusView!
    
    var locationManager: CLLocationManager?
    
    var annotationViewImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        betshopManager = BetshopManager()
        betshopManager!.delegate = self
        
        statusView.delegate = self
        statusView.layer.cornerRadius = 10
        statusView.layer.masksToBounds = true
        
        infoView.layer.cornerRadius = 25
        infoView.layer.masksToBounds = true
        infoView.nameLabel.adjustsFontSizeToFitWidth = true
        infoView.addressLabel.adjustsFontSizeToFitWidth = true
        infoView.cityCountyLabel.adjustsFontSizeToFitWidth = true
        
        mapView.delegate = self
        mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 48.137154, longitude: 11.576124), span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1)), animated: false)
        mapView.showsUserLocation = true
        mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: "BetshopAnnotationView")
        mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: "BetshopClusterAnnotationView")
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        
        annotationViewImage = UIImage(named: "Asset 54")!.getAnnotationViewImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        betshopManager!.downloadBetshops()
    }
    
    // When the user taps on the X button inside the infoView, we want to hide the infoView and simultaneously deselect the selected annotation
    @IBAction func closeInfoView(_ sender: Any) {
        for annotation in mapView.selectedAnnotations {
            mapView.deselectAnnotation(annotation, animated: true)
        }
    }
    
    // When the user asks for directions to the betshop, we open Apple Maps with the desired route
    @IBAction func openDirections(_ sender: Any) {
        if let betshop = infoView.selectedBetshop {
            let coordinate = CLLocationCoordinate2D(latitude: betshop.Location.Lat, longitude: betshop.Location.Lng)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
            mapItem.name = betshop.Name
            
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
    }
    
    // BetshopManager has started loading data, update the statusView
    func didStartLoadingData() {
        statusView.setState(state: .LoadingData)
    }
    
    // BetshopManager failed to download and parse betshops => we show the error
    func didFailToLoadBetshops() {
        print("Failed to load betshops")
        statusView.setState(state: .NoInternet)
    }
    
    // User has requested to try again
    func userRequestedReload() {
        betshopManager!.downloadBetshops()
    }
    
    // BetshopManager has successfully downloaded and parsed the betshops => we remove old betshops from the map and show the new ones
    func didUpdateBetshops(betshops: [Betshop]) {
        DispatchQueue.main.async {
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.addAnnotations(self.betshopManager!.getAnnotations())
            
            self.statusView.hide()
        }
    }
    
    // Creating the view for the annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation { return nil }
        
        var annotationViewIdentifier = "BetshopAnnotationView"
        
        if annotation is MKClusterAnnotation {
            annotationViewIdentifier = "BetshopClusterAnnotationView"
        }
        
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationViewIdentifier)!
        
        annotationView.annotation = annotation
        annotationView.clusteringIdentifier = "Betshop"
        annotationView.image = annotationViewImage
        annotationView.centerOffset = CGPoint(x: 0, y: -annotationView.image!.size.height / 2)
        
        return annotationView
    }

    // When the user selects a betshop, we change the image of the MKAnnotationView, show the infoView and center the map
    // If the user has pressed on the MKClusterAnnotation (many clustered betshops), then we zoom on that location
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation { return }
        
        if view.annotation is MKClusterAnnotation {
            let viewAnnotation = view.annotation as! MKClusterAnnotation
            
            mapView.setRegion(MKCoordinateRegion(center: viewAnnotation.coordinate, span: MKCoordinateSpan(latitudeDelta: mapView.region.span.latitudeDelta * 0.25, longitudeDelta: mapView.region.span.longitudeDelta * 0.25)), animated: true)
            mapView.deselectAnnotation(view.annotation, animated: false)
            
            return
        }
        
        view.image = UIImage(named: "Asset 55")
        
        let betshop = (view.annotation as! BetshopAnnotation).betshop
        var calendar = Calendar.current
        // Check what time it is at the location of the desired betshop
        if let timeZone = betshopManager?.timeZoneForBetshop[betshop.Id] {
            calendar.timeZone = timeZone
        }
        infoView.present(betshop: betshop, calendar: calendar)
        self.mapView.setRegion(MKCoordinateRegion(center: view.annotation!.coordinate, span: mapView.region.span), animated: true)
    }
    
    // When the user deselects a betshop, we want to show the unselected image and hide the infoView
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
            return
        }
        
        view.image = annotationViewImage
        
        infoView.hide()
    }
    
    // When we get the user's permission to access location data, we request a one time location (we don't need continuous location updates => better battery efficiency)
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager?.requestLocation()
        }
    }
    
    // Location manager gave us a new location => we center the map on the user's location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            mapView.setRegion(MKCoordinateRegion(center: location.coordinate, span: mapView.region.span), animated: true)
        }
    }
    
    // Location manager failed to get a location
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    // When a selected annotation is about to be clustered, deselect it
    func mapView(_ mapView: MKMapView, clusterAnnotationForMemberAnnotations memberAnnotations: [MKAnnotation]) -> MKClusterAnnotation {
        for selectedAnnotation in mapView.selectedAnnotations {
            for memberAnnotation in memberAnnotations {
                if memberAnnotation.isEqual(selectedAnnotation) {
                    mapView.deselectAnnotation(selectedAnnotation, animated: true)
                    return MKClusterAnnotation(memberAnnotations: memberAnnotations)
                }
            }
        }
        return MKClusterAnnotation(memberAnnotations: memberAnnotations)
    }
}

extension UIImage {
    func getAnnotationViewImage() -> UIImage {
        let size = UIImage(named: "Asset 55")!.size
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        draw(in: CGRect(x: (size.width - self.size.width) / 2, y: size.height - self.size.height, width: self.size.width, height: self.size.height))

        return UIGraphicsGetImageFromCurrentImageContext()!
    }
}
